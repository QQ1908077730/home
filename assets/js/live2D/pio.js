
//是否已显示卡通人物
var closeModules = true;

var isLike = false;

var timeoutId;


function isLikePio() {
	if(!closeModules){
		return
	}
	if(timeoutId){
		//取消 setTimeout 的执行
		clearTimeout(timeoutId);
	}
	isLike = !isLike;
	// 找到要修改的元素
	var pioDialog = document.querySelector('.pio-dialog');
	// 激活显示卡通对话框
	pioDialog.classList.add('active');
	if(isLike){
		pioDialog.textContent = "我代表程序员鹏peng，谢谢你的点赞！";
	}else{
		pioDialog.textContent = "你真的忍心取消点赞嘛 呜呜呜QWQ！";
	}
	timeoutId = setTimeout(function(){
		// 将元素的类更改为 "pio-dialog"
		pioDialog.classList.remove('active');
	},5000)
	
	//鼠标悬浮点赞时，延迟提示
	isClicklikeButton = true;
	setTimeout(function(){
		isClicklikeButton = false;
	},1000)
	
	// 重置定时器，卡通人物空闲30秒后自动说话
	clearIntervalId();
}

function contentPio(text) {
	if(!closeModules){
		return
	}
	if(timeoutId){
		//取消 setTimeout 的执行
		clearTimeout(timeoutId);
	}
	// 找到要修改的元素
	var pioDialog = document.querySelector('.pio-dialog');
	// 激活显示
	pioDialog.classList.add('active');
	pioDialog.textContent = text;
	timeoutId = setTimeout(function(){
		// 将元素的类更改为 "pio-dialog"
		pioDialog.classList.remove('active');
	},5000)

	// 重置定时器，卡通人物空闲30秒后自动说话
	clearIntervalId();
}

// 是否点击了点赞
var isClicklikeButton = false;

// 获取点赞按钮元素
var likeButton = document.getElementById('like-button');

// 添加鼠标悬停事件监听器
likeButton.addEventListener('mouseover', function() {
  //likeButton.classList.add('hovered'); // 添加悬停时的样式
  if(isClicklikeButton){
	  return;
  }
  if(isLike){
	  contentPio("求你了~别取消点赞！QWQ");
  }else{
	  contentPio("请点个赞吧！😊");
  }
  
  // 重置定时器，卡通人物空闲15秒后自动说话
  clearIntervalId();
});

// 初始设置定时器，每隔30秒卡通人物自动说话
var intervalId; // 时间单位为毫秒

// 重置定时器，卡通人物空闲15秒后自动说话
function clearIntervalId(){
	clearInterval(intervalId); // 清除现有的定时器
	intervalId = setInterval(function() {
		fetch('https://v1.hitokoto.cn').then(response => response.json()).then(data => {
			contentPio(data.hitokoto)
		}).catch(console.error)
	}, 20000); // 重新设置为每隔20秒执行一次
}
// 添加鼠标移出事件监听器
// likeButton.addEventListener('mouseout', function() {
//   likeButton.classList.remove('hovered'); // 移除悬停时的样式
//   isClicklikeButton = false;
// });

/* ----
# Pio Plugin
# By: Dreamer-Paul
# Last Update: 2021.3.3

一个支持更换 Live2D 模型的 Typecho 插件。
---- */

var Paul_Pio = function (prop) {
	
	//对话框是否已激活
	var isActionWelcome = false;
	
	//卡通人物显示成功后清除定时任务
	var clearCanvasDrawnInterval;
	
	//是否点击了返回主页按钮
	var isClickButtons = false;
	
    var that = this;
	
	// 默认模型
	var index = 1;

    var current = {
        idol: index,
        menu: document.querySelector(".pio-container .pio-action"),
        canvas: document.getElementById("pio"),
        body: document.querySelector(".pio-container"),
		//root: "#hero",
		root: document.location,
        //root: document.location.protocol +'//' + document.location.hostname +'/home/'
    };

    /* - 方法 */
    var modules = {
        // 更换模型

        idol: function () {
            current.idol < (prop.model.length - 1) ? current.idol++ : current.idol = 0;
            return current.idol;
        },
        // 创建内容
        create: function (tag, prop) {
            var e = document.createElement(tag);
            if(prop.class) e.className = prop.class;
            return e;
        },
        // 随机内容
        rand: function (arr) {
            return arr[Math.floor(Math.random() * (arr.length - 1))];
        },
        // 创建对话框方法
        render: function (text) {
            if(text.constructor === Array){
                dialog.innerText = modules.rand(text);
            }
            else if(text.constructor === String){
                dialog.innerText = text;
            }
            dialog.classList.add("active");
            clearTimeout(this.t);
            this.t = setTimeout(function () {
                dialog.classList.remove("active");
            }, 5000);
			timeoutId = this.t;
			
			// 重置定时器，卡通人物空闲15秒后自动说话
			clearIntervalId();
        },
        // 移除方法
        destroy: function () {
            that.initHidden();
            // localStorage.setItem("posterGirl", 0);
        },
        // 是否为移动设备
        isMobile: function () {
            var ua = window.navigator.userAgent.toLowerCase();
            ua = ua.indexOf("mobile") || ua.indexOf("android") || ua.indexOf("ios");
			
			//移动端能互动卡通人物
			current.body.style.pointerEvents = 'auto';
			current.canvas.style.pointerEvents = 'auto';
			current.menu.style.pointerEvents = 'auto';
			
			// 判断是否为移动端
			if(window.innerWidth < 500 || ua !== -1){
				// // 找到要设置不透明度的元素
				// var pioAction = document.querySelector('.pio-action');
				// // 设置元素的不透明度为完全不透明
				// pioAction.style.opacity = 1;
				// pioAction.style.top = 'auto';
				// pioAction.style.pointerEvents = 'auto';
				
				//var pioContainerRight = document.querySelector('pio-container right');
				
				//左边按钮y间距
				current.menu.style.top = 'auto';
				dialog.style.bottom = '13em'
				dialog.style.minWidth = '9em'
			}
			// 返回false，移动端则显示卡通人物
			return false;
			
			// 判断是否为移动端
            // return window.innerWidth < 500 || ua !== -1;
        },
		clickButtons: function(){
			isClickButtons = true;
			setTimeout(function(){
				isClickButtons = false;
			},1000)
		},
		canvasDrawnInterval: function(){
			//console.log(window.Live2D.getGL)
			//默认模型未加载完成
			window.isLoadlive2d = false;
			//等待卡通人物加载完成后显示说话内容
			clearCanvasDrawnInterval = setInterval(function() {
				if(window.isLoadlive2d){
					if(!isActionWelcome){
						action.welcome();
						isActionWelcome = !isActionWelcome;
					}else{
						if (current.idol === 0){
						    modules.render("你好！我是雷姆，接下来由我来介绍这个页面！");
						}else if (current.idol === 2){
						    modules.render("你好！我是血小板，接下来由我来介绍这个页面！");
						}else {
						    modules.render("我终于见到你了~ 接下来由我来介绍这个页面！");
						}
					}
					clearInterval(clearCanvasDrawnInterval); // 清除现有的定时器
				}
			}, 500);
		}
    };
    this.destroy = modules.destroy;

    var elements = {
        home: modules.create("span", {class: "pio-home"}),
        skin: modules.create("span", {class: "pio-skin"}),
        info: modules.create("span", {class: "pio-info"}),
        sentence: modules.create("span", {class: "pio-sentence"}),
        close: modules.create("span", {class: "pio-close"}),

        show: modules.create("div", {class: "pio-show"})
    };

    var dialog = modules.create("div", {class: "pio-dialog"});
    current.body.appendChild(dialog);
    current.body.appendChild(elements.show);
	
	var isclickButtons = false;
	
    /* - 提示操作 */
        var action = {
        // 欢迎
         welcome: function () {
            if(document.referrer !== "" && document.referrer.indexOf(current.root) === -1){
                var referrer = document.createElement('a');
                referrer.href = document.referrer;
                // prop.content.referer ? modules.render(prop.content.referer.replace(/%t/, "“" + referrer.hostname + "”")) : modules.render("欢迎来自 “" + referrer.hostname + "” 的朋友！");
                prop.content.referer ? modules.render(prop.content.referer.replace(/%t/, "“" + referrer.hostname + "”")) : modules.render("欢迎来到\"程序员鹏peng\"的博客");
            }else if(prop.tips){
                var text, hour = new Date().getHours();

                if (hour > 22 || hour <= 5) {
                    text = '你是夜猫子呀？这么晚还不睡觉，明天起的来嘛';
                }
                else if (hour > 5 && hour <= 8) {
                    text = '早上好！';
                }
                else if (hour > 8 && hour <= 11) {
                    text = '上午好！工作顺利嘛，不要久坐，多起来走动走动哦！';
                }
                else if (hour > 11 && hour <= 13) {
                    text = '中午了，工作了一个上午，现在是午餐时间！';
                }
                else if (hour > 13 && hour <= 17) {
                    text = '午后很容易犯困呢，今天的运动目标完成了吗？';
                }
                else if (hour > 17 && hour <= 19) {
                    text = '傍晚了！窗外夕阳的景色很美丽呢，最美不过夕阳红~';
                }
                else if (hour > 19 && hour <= 21) {
                    text = '晚上好，今天过得怎么样？';
                }
                else if (hour > 21 && hour <= 23) {
                    text = '已经这么晚了呀，早点休息吧，晚安~';
                }
                else{
                    text = "奇趣保罗说：这个是无法被触发的吧，哈哈";
                }
                modules.render(text);
            }else{
                modules.render(prop.content.welcome || "欢迎来到\"程序员鹏peng\"的博客");
            }
        },
        // 触摸
        touch: function () {
            current.canvas.onclick = function () {
                if (current.idol === 0){
                    modules.render(prop.content.touch || ["需要我为您介绍一下这个页面的功能吗？","有任何反馈或建议，请告诉我，雷姆会认真听取的。",
					"希望您在这里度过愉快的时光。","有什么需要我推荐的吗？","今天也要加油哦，雷姆会一直支持您的。",
					"请注意休息，不要太累哦。","如果您喜欢这个网站，请记得点赞+收藏哦！",""]);
                }else if (current.idol === 2){
                    modules.render(prop.content.touch || ["准备好了吗？那我们开始吧！",
					"这里有伤口！大家快来帮忙！","要好好工作哦！","要好好工作哦！","加油！我们一定能做到的！","哇，好大的伤口啊！",
					"我们来修复受损的血管！","一、二、三，开始！","如果您喜欢这个网站，请记得点赞+收藏哦！",""]);
                }else {
                    modules.render(prop.content.touch || 
					["你在干什么？","干嘛动我呀！小心我咬你！","非礼呀！救命！","再摸的话我可要报警了⌇●﹏●⌇",
					"是···是不小心碰到了吧~~~","110 吗，这里有个变态一直在摸我(ó﹏ò)", "HENTAI!", "不可以这样欺负我啦！",
					"萝莉控是什么呀？", "你看到我的小熊了吗？","如果您喜欢这个网站，请记得点赞+收藏哦！",""]);
                }
            };
			
        },
        // 右侧按钮
         buttons: function () {
			 
             // 返回首页
             elements.home.onclick = function () {
                //location.href = current.root;
				//跳转至顶部
				window.scrollTo({
					top: 0,
					behavior: 'smooth' // 平滑滚动效果
				});
             };
             elements.home.onmouseover = function () {
                 modules.render(prop.content.home || "点击这里回到首页！");
             };
             current.menu.appendChild(elements.home);

             // 更换模型
             elements.skin.onclick = function () {
                 loadlive2d("pio", prop.model[modules.idol()]);
                 //prop.content.skin && prop.content.skin[1] ? modules.render(prop.content.skin[1]) : modules.render("我终于见到你了~");
				 dialog.classList.remove("active")
				 //等待人物显示完成后显示对话框
				 modules.canvasDrawnInterval();
				 //点击了模型左边按钮，则延迟触发鼠标悬浮事件
				 modules.clickButtons();
				 
             };
             elements.skin.onmouseover = function () {
				 if(isClickButtons){
					 return;
				 }
				 modules.render("要换下一个模型吗？")
                 //prop.content.skin && prop.content.skin[index] ? modules.render(prop.content.skin[index]) : modules.render("你要换下一个模型吗？");
             };
             if(prop.model.length > 1) current.menu.appendChild(elements.skin);

             // 关于我
             elements.info.onclick = function () {
                 window.open("https://blog.csdn.net/qq_45415736?type=blog");
             };
             elements.info.onmouseover = function () {
                 modules.render("想了解更多关于程序员鹏peng的信息吗？");
             };
             current.menu.appendChild(elements.info);

             // 一言
             elements.sentence.onclick = function () {
                modules.render(fetch('https://v1.hitokoto.cn').then(response => response.json())
						.then(data => {
								 const hitokoto = document.querySelector('.pio-dialog')
								 hitokoto.href = 'https://hitokoto.cn/?uuid=' + data.uuid
								 hitokoto.innerText = data.hitokoto })
						.catch(console.error));
						
				//点击了模型左边按钮，则延迟触发鼠标悬浮事件
				modules.clickButtons();
            };
			elements.sentence.onmouseover = function () {
				if(isClickButtons){
					return;
				}
				modules.render("我从青蛙王子那里听到了不少人生经验。");
			};
            current.menu.appendChild(elements.sentence);


             // 关闭看板娘
            elements.close.onclick = function () {
				closeModules = false;
                modules.destroy();
            };
            elements.close.onmouseover = function () {
                modules.render(prop.content.close || "QWQ 下次再见吧~");
            };
            current.menu.appendChild(elements.close);
			
			
         },
        custom: function () {
            prop.content.custom.forEach(function (t) {
                if(!t.type) t.type = "default";
                var e = document.querySelectorAll(t.selector);

                if(e.length){
                    for(var j = 0; j < e.length; j++){
                        if(t.type === "read"){
                            e[j].onmouseover = function () {
                                modules.render("想阅读 %t 吗？".replace(/%t/, "“" + this.innerText + "”"));
                            }
                        }
                        else if(t.type === "link"){
                            e[j].onmouseover = function () {
                                modules.render("想了解一下 %t 吗？".replace(/%t/, "“" + this.innerText + "”"));
                            }
                        }
                        else if(t.text){
                            e[j].onmouseover = function () {
                                modules.render(t.text);
                            }
                        }
                    }
                }
            });
        }
    };

    /* - 运行 */
    var begin = {
        static: function () {
            current.body.classList.add("static");
        },
        fixed: function () {
            action.touch(); action.buttons();
        },
        draggable: function () {
            action.touch(); action.buttons();

            var body = current.body;
            body.onmousedown = function (downEvent) {
                var location = {
                    x: downEvent.clientX - this.offsetLeft,
                    y: downEvent.clientY - this.offsetTop
                };

                function move(moveEvent) {
                    body.classList.add("active");
                    body.classList.remove("right");
                    body.style.left = (moveEvent.clientX - location.x) + 'px';
                    body.style.top  = (moveEvent.clientY - location.y) + 'px';
                    body.style.bottom = "auto";
                }

                document.addEventListener("mousemove", move);
                document.addEventListener("mouseup", function () {
                    body.classList.remove("active");
                    document.removeEventListener("mousemove", move);
                });
            };
        }
    };

    // 运行
    this.init = function (onlyText) {
        if(!(prop.hidden && modules.isMobile())){
            if(!onlyText){
				//随机模型
                // loadlive2d("pio", prop.model[Math.floor(Math.random()*(prop.model.length))]);
				var idol = localStorage.getItem("idol");
				if(idol == null){
					current.idol = Math.floor(Math.random()*(prop.model.length));
				}else{
					current.idol = idol;
					modules.idol();
				}
				
                loadlive2d("pio", prop.model[current.idol]);
				localStorage.setItem("idol",current.idol);
				
            }
			
			
			
            switch (prop.mode){
                case "static": begin.static(); break;
                case "fixed":  begin.fixed(); break;
                case "draggable": begin.draggable(); break;
            }

            if(prop.content.custom) action.custom();
			
			modules.canvasDrawnInterval();
			
        }
    };

    // 隐藏状态
    this.initHidden = function () {
        current.body.classList.add("hidden");
        dialog.classList.remove("active");

        elements.show.onclick = function () {
			closeModules = true;
			//clearTimeout(timeoutId);
            current.body.classList.remove("hidden");
            // localStorage.setItem("posterGirl", 1);
            // that.init();
        }
		elements.show.onmouseover = function () {
		    modules.render(prop.content.close || "我藏~ 你想我了嘛~");
		};
    }

    localStorage.getItem("posterGirl") == 0 ? this.initHidden() : this.init();
	
	// 使用 call 方法将 current 绑定到 test 函数的 this 上
	//test.call(current);
};
// 调用 Paul_Pio 函数
//Paul_Pio();
// 监听窗口大小改变事件
window.addEventListener('resize', function() {
	var ua = window.navigator.userAgent.toLowerCase();
	ua = ua.indexOf("mobile") || ua.indexOf("android") || ua.indexOf("ios");
	// 找到要设置元素
	var menu = document.querySelector(".pio-container .pio-action");
	var canvas = document.getElementById("pio");
	var body = document.querySelector(".pio-container");
	var pioDialog = document.querySelector('.pio-dialog');
	if(window.innerWidth < 500 || ua !== -1){
		//左边按钮y间距
		menu.style.top = '0em';
		pioDialog.style.bottom = '13em'
		pioDialog.style.minWidth = '9em'
	}else{
		//电脑端
		menu.style.top = '3em';
		pioDialog.style.bottom = '20em'
		pioDialog.style.minWidth = '12em'
	}
});